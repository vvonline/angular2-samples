import { Component, OnInit, OnChanges, Input, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Hero } from '../shared/hero.model';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit, OnChanges {

  @Input()
  private hero: Hero;

  private heroForm: FormGroup;

  constructor(private _fb: FormBuilder) { }

  ngOnChanges(changes: SimpleChanges): void {
    for (let propertyName in changes) {
      if(this.heroForm && propertyName === 'hero') {
        this.initializeForm(this.hero);
      }
    }
  }

  ngOnInit() {
    this.createForm();
    this.initializeForm(this.hero);
  }

  createForm() {
    this.heroForm = this._fb.group({
      name: ['', Validators.required]
    });
  }

  initializeForm(hero: Hero){
    this.heroForm.patchValue({ name: hero.name });
  }

}
