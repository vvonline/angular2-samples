import { Injectable } from '@angular/core';

import { Hero } from './hero.model';

import { heroes } from '../../mock/data.model';

@Injectable()
export class HeroService {

  constructor() { }

  getHeroes() : Array<Hero> {
    return heroes;
  }
}
