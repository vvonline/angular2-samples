import { Component, OnInit } from '@angular/core';

import { HeroService } from '../shared/hero.service';
import { Hero } from '../shared/hero.model';

@Component({
  selector: 'app-hero-list',
  templateUrl: './hero-list.component.html',
  styleUrls: ['./hero-list.component.css']
})
export class HeroListComponent implements OnInit {

  private heroes: Array<Hero>;
  private selectedHero: Hero;

  constructor(private _hs: HeroService) { }

  ngOnInit() {
    this.heroes = this._hs.getHeroes();
  }

  selectHero(hero: Hero) {
    this.selectedHero = hero;
  }

}
