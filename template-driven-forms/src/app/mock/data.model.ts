import { Hero } from '../hero/shared/hero.model';

export const heroes: Array<Hero> = [
    {
        id: 1,
        name: 'Whirlwind'
    },
    {
        id: 2,
        name: 'Bombastic'
    },
    {
        id: 3,
        name: 'Magneta'
    }
];
